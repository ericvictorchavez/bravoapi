package com.fruits_api.fruits_api.controllers;

import java.util.List;
import java.util.Optional;

import com.fruits_api.fruits_api.models.Fruit;
import com.fruits_api.fruits_api.repositories.FruitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("fruits")
public class FruitController {

    @Autowired
    private FruitRepository fruitRepo;

    @GetMapping("/all")
    private ResponseEntity<List<Fruit>> getFruits() {
        return ResponseEntity.ok (fruitRepo.findAll());
    }
    @PostMapping("create")
    public ResponseEntity <Fruit> createFruit(@RequestBody Fruit fruit) {
        return ResponseEntity.ok(fruitRepo.save(fruit));
    }

    @PutMapping("update/{id}")
    public ResponseEntity<Fruit> update(@PathVariable Long id,@RequestBody Fruit fruit) {

        Optional<Fruit> f =fruitRepo.findById(id);
        fruit.setId(f.get().getId());

        fruitRepo.save(fruit);
        return ResponseEntity.ok(fruit);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<String> deleteFruit(@PathVariable Long id) {
        Optional <Fruit> fruitDelete = fruitRepo.findById(id);
        if (fruitDelete.isPresent()) {
            fruitRepo.delete(fruitDelete.get());
            return ResponseEntity.ok("Deleted");
        }
        return ResponseEntity.ok("Not Found");
    }
    
}
