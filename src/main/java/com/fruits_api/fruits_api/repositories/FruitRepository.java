package com.fruits_api.fruits_api.repositories;

import com.fruits_api.fruits_api.models.Fruit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FruitRepository extends JpaRepository<Fruit, Long>{
    
}